package imd.ufrn.br.gasolinaalcool;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    private EditText gasolinaText, alcoolText;
    private TextView resultadoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gasolinaText = (EditText) findViewById(R.id.gasolinaText);
        alcoolText = (EditText) findViewById(R.id.alcoolText);
        resultadoView = (TextView) findViewById(R.id.resultadoView);

    }

    public void calcular(View view) {

        double valorGasolina = Double.parseDouble(gasolinaText.getText().toString());
        double valorAlcool = Double.parseDouble(alcoolText.getText().toString());
        double resultado = (valorAlcool * 100)/valorGasolina;

        if(resultado > 70)
            resultadoView.setText("Gasolina compensa!");
        else if( resultado == 70)
            resultadoView.setText("Tanto faz!");
        else
            resultadoView.setText("Álcool compensa!");

    }
}
